# Curso de programación

Un curso de Python orientado a los estudiantes de la Facultad de Meteorologia de la UNALM , investigadores y profesionales que trabajen o desean trabajar con las ciencias de la Atmosfera. 

<table align="left"> 
  <tr>
    <td> <b>Docentes</b></td>
    <td>xxxxxxxxxxxxxxxx</td>
    <td>XXXxxxxxxxxxxxxx</td>
    <td>XXXxxxxxxxxxxxxx</td>
  <tr>
</table>
<br>
<br>


### Resumen
***
En las últimas décadas la computación se ha convertido en un pilar de la ingeniería y el desarrollo científico-tecnológico. En la mayoría de las áreas, el trabajo computacional es un complemento fundamental de la experimentación tradicional y de la teoría, ya que cada vez más se involucra simulaciones numéricas y modelado computacional. Frecuentemente, la tarea de los profesionales involucra el uso de aplicaciones específicas que requieren un gran trabajo de procesamiento de los datos de entrada y un post-procesamiento de los resultados utilizando otras herramientas.

Este curso brinda una introducción sólida al poderoso lenguaje de programación Python (http://python.org) y a las herramientas fundamentales del "ecosistema científico" (Jupyter, Numpy, Matplotlib, Scipy, entre otras) que conforman un entorno de programación interactivo de vanguardia, simple, libre, gratuito y multiplataforma.


### Sobre el docente
***

n DOCENTE DESCRIPCION
Más información en su []()  

### Programa
***

[Clase 1]():

    ARQUITECTURA DE LA COMPUTADORA
    ●   La unidad central de proceso y memoria
    ●   Funcionamiento de la computadora e internet (hardware y software)

    INTRODUCCIÓN A LA PROGRAMACIÓN
    ●   Lenguajes de programación
    ●   Programación orientada al desarrollo web y desktop
    ●   Programación orientada al análisis de datos


[Clase 2](https://github.com/ryali93/UNMSM_programacion2019/blob/master/python/clase02_SintaxisPython.ipynb):

    TÉCNICAS DE PROGRAMACIÓN 
    ●   Programación modular
    ●   Programación estructurada
    ●   Programación orientada a objetos
    ●   Diagramas de flujo y Pseudocódigo

    CONCEPTOS FUNDAMENTALES
    ●   Tipos de datos
    ●   Identificadores, constantes y variables.


[Clase 3](https://github.com/ryali93/UNMSM_programacion2019/blob/master/python/clase03_EstructuraDatos.ipynb):

        SINTAXIS Y ESTRUCTURAS DE DATOS
    ●   Expresiones Aritméticas y Lógicas
    ●   Programas secuenciales
    ●   Estructura general del lenguaje Python
    ●   Tuplas, Listas, Diccionarios
    ●   Operaciones: Actualización, inserción, eliminación, modificación


[Clase 4](https://github.com/ryali93/UNMSM_programacion2019/blob/master/python/clase04_Condicionales.ipynb):

        ESTRUCTURAS DE CONTROL SELECTIVAS
    ●   La estructura simple (SI-ENTONCES)
    ●   La sentencia if, elif
    ●   Estructuras Anidadas
    ●   Estructuras múltiples (SI-MÚLTIPLE)


[Clase 5](https://github.com/ryali93/UNMSM_programacion2019/blob/master/python/clase05_Bucles.ipynb):

    ESTRUCTURAS DE CONTROL REPETITIVAS
    ●   Bucles y loops
    ●   Sentencia WHILE
    ●   Sentencia FOR
    ●   Contadores y acumuladores
    ●   Instrucciones BREAK, PASS, CONTINUE


[Clase 6](https://github.com/ryali93/UNMSM_programacion2019/blob/master/python/clase06_Funciones.ipynb):

        FUNCIONES
    ●   Parámetros y argumentos
    ●   Funciones predefinidas
    ●   Funciones definidas por el usuario
    ●   Importación de paquetes

[Clase 7](https://github.com/ryali93/UNMSM_programacion2019/blob/master/python/clase07_ManipulacionArchivos.ipynb):

    ARCHIVOS
    ●   Lectura de archivos
    ●   Sentencia WITH, AS
    ●   Modificación y creación de archivos
    ●   Ejemplos

[Clase 8]():

    PRIMER EXAMEN PARCIAL

[Clase 9](https://github.com/ryali93/UNMSM_programacion2019/blob/master/python/clase09_numpy.ipynb):

     ANÁLISIS DE DATOS
    ●   Introducción a Numpy.
    ●   Subsetting Numpy Arrays
    ●   Estadísticas básicas en numpy
    ●   Introducción a Pandas.
    ●   Matrices y Data Frames

[Clase 10](https://github.com/ryali93/UNMSM_programacion2019/blob/master/python/clase10_pandas.ipynb):

    TRATAMIENTO DE DATOS
    ●   Creación de data.frames
    ●   Reshaping data
    ●   Resumen de datos.
    ●   Manejo de valores vacíos.
    ●   Agrupamiento de datos

[Clase 11](https://github.com/ryali93/UNMSM_programacion2019/blob/master/python/clase11_Matplotlib.ipynb):

    REPRESENTACIÓN GRÁFICA
    ●   Introducción a Matplotlib
    ●   Manipulación de datos
    ●   Indexación avanzada
    ●   Agrupación y filtrado
    ●   Gráficos básicos

[Clase 12](https://github.com/ryali93/UNMSM_programacion2019/blob/master/python/clase12_spatial.ipynb):

    ÁLGEBRA LINEAL EN PYTHON 
    ●   Placeholders.
    ●   Creación de arrays.
    ●   Ordenamientos de arrays.
    ●   Subsetting, slicing, indexing
    ●   manipulación de arrays.


[Clase 13](https://github.com/ryali93/UNMSM_programacion2019/blob/master/python/clase13_numpyEstadistica.ipynb):

    GEOPANDAS - MANEJO DE DATOS VECTORIALES EN PYTHON
    ●   WKT
    ●   GeoJSON
    ●   Operaciones geoespaciales.
    ●   Placeholder (shapely)

[Clase 14](https://github.com/ryali93/UNMSM_programacion2019/blob/master/python/clase14_EarthEngine.ipynb):

    GOOGLE EARTH ENGINE
    ●   Programación cliente-servidor
    ●   Estructuras de datos en GEE.
    ●   ee.Image vs ee.ImageCollection
    ●   ee.Feature vs ee.FeatureCollection.

[Clase 15]():

    INTRODUCCIÓN A MACHINE LEARNING
    ●   Análisis de Componentes principales
    ●   Clasificación supervisada
    ●   Clasificación no supervisada

[Clase 16]():
    
    EXAMEN FINAL


### Licencia
***

<a  rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" align="left" /></a>
<br>
<br>


### Créditos
***

Parte de este material ha sido adaptado de :

* `https://github.com/ryali93/UNMSM_programacion` de Roy Yali UNMSM
* `Scipy Lectures <http://scipy-lectures.github.io/>`
* `Scientific Python Lectures <https://github.com/jrjohansson/scientific-python-lectures>`
  de J.R. Johansson
* `El tutorial de Python <http://docs.python.org.ar/tutorial/2/contenido.html>`
  traducción al castellano por la comunidad `Python Argentina <http://python.org.ar/>`
* `Matplotlib tutorial <http://webloria.loria.fr/~rougier/teaching/matplotlib/>` por Nicolas P. Rougier
* `Numba vs Python, take 2 <http://nbviewer.ipython.org/url/jakevdp.github.io/downloads/notebooks/NumbaCython.ipynb>` de Jake Vanderplasf


*En desarrollo
